<?php

/**
 * Implement HOOK_rules_action_info.
 */
function fbpp_rules_action_info() {
  $actions = array(
    'fbpp_post_to_wall_as_admin' => array(
      'label' => t("Post an update on facebook page as admin."),
      'group' => t('Facebook'),
      'parameter' => array(
        'pageid' => array(
          'type' => 'token',
          'label' => t('Page'),
          'options list' => 'fbpp_pages_select_list',
          'optional' => FALSE,
        ),
        'message' => array(
          'type' => 'text',
          'optional' => TRUE,
          'label' => t('Message'),
          'description' => t("The message to post on Facebook wall."),
        ),
        'link' => array(
          'type' => 'text',
          'optional' => TRUE,
          'label' => t('Link to post'),
          'description' => t("The link to post."),
        ),
        'name' => array(
          'type' => 'text',
          'optional' => TRUE,
          'label' => t('Name of the link'),
          'description' => t("The name of the link. If not specified, default would be used."),
        ),
        'caption' => array(
          'type' => 'text',
          'optional' => TRUE,
          'label' => t('Caption of the link.'),
          'description' => t("The caption of the link (appears beneath the link name). If not specified, default would be used."),
        ),
        'description' => array(
          'type' => 'text',
          'optional' => TRUE,
          'label' => t('Description of the link.'),
          'description' => t("A description of the link (appears beneath the link caption). If not specified, default would be used."),
        ),
        'picture' => array(
          'type' => 'text',
          'optional' => TRUE,
          'label' => t('Picture of the link.'),
          'description' => t("A URL to the thumbnail image used in the link post. If not specified, default would be used."),
        ),
      ),
    ),
  );
  return $actions;
}

function fbpp_post_to_wall_as_admin($pageid, $message, $link, $name, $caption, $description, $picture) {
  // Get the token for the page.
  //$pages = variable_get('fbpp_settings', array());
  //if (isset($pages[$pageid]) && isset($page[$pageid]['access_token'])) {
    //facebook_page_rules_post($message, $link, $name, $caption, $description, $access_token);
  //}
  
  
  //$access_token = variable_get('facebook_page_rules_page_token', NULL);
  fbpp_post($pageid, $message, $link, $name, $caption, $description, $picture);
}
